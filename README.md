# Blockland DLL Tools has moved to notabug.org due to GitLab's arbitrary denial of service to users who protect their personal information through safer web browser configurations
# This repository is now located here: https://notabug.org/Queuenard/blockland-DLL-tools


# Blockland DLL list

List of Blockland DLL loaders and DLLs

Current Blockland version: **r2033**

# Index
- [Introduction](#introduction)
- [Loaders and patched executables](#loaders-and-patched-executables)
- [DLLs](#dlls)
- [DLLs (v20)](#dlls-v20)
- [DLLs (abandoned)](#dlls-abandoned)
- [DLLs (archived)](#dlls-archived)
- [Libraries](#libraries)
- [Uncategorized](#uncategorized)

# Introduction

Blockland DLLs can modify the game in ways not possible with add-ons and TorqueScript. They can be essentially add-ons on their own or resources for scripters to facilitate internal engine access. For example, how can I have a script detect if a player moves? Rather than polling every few milliseconds, a DLL can inject itself and trigger a TorqueScript callback whenever the engine detects a player movement.

## **Patching Blockland**

Blockland needs an entry in its import table to load external DLLs. While loaders (only one works now: RedBlocklandLoader.dll) provide a patched Blockland.exe, it is possible to generate your own:

- Using StudPE and adding RedBlocklandLoader.dll to the import table,

or

- Using BL-patcher.py, located in this repository, which was designed to imitate the same routines as StudPE and produce a near byte-for-byte match. You'll need the **pefile** python package, but no other documentation is provided at this time.

## **Loaders and patched executables**

The loader is attached to the Blockland process and loads DLLs located in a certain folder. For both of the known public loaders, modules are loaded from the modules/ folder and executed immediately before the engine calls Sim::init.

**Available version** - the version of Blockland that is provided by this repository. Put simply, it's normally a base executable with the respective DLL loader attached inside the import table.

**Compatible version** - the version of Blockland for which the DLL works, provided the attached Blockland executable has the respective DLL in its import table. Note that some DLLs may work in higher Blockland versions than the version distributed in the respective project.

| Name | URL | Available version | Compatible version | Notes |
| ------ | ------ | ------ | ------ | ------ |
| RedBlocklandLoader | https://gitlab.com/Redo0/redblocklandloader | r2012 | **r2033** ✓ | Provides an older Blockland version, but the DLL continues to work on r2033 with a patched-executable. |
| RedBlocklandLoader (Eagle517)| https://gitlab.com/Eagle517/redblocklandloader | **r2033** ✓ | **r2033** ✓ | 3rd-party  update for RedBlocklandLoader due to May 2020 game updates.
| BlocklandLoader | https://github.com/qoh/BlocklandLoader | r1997 | r2001 | Not updated and no longer working: Torque function signatures changed in May 2020. |



## **DLLs**

**Compatible version** - the highest Blockland version with which the DLL works, as confirmed by my own tests or as claimed by the project owner.

**Ready-to-install** - whether a compiled DLL is provided and ready for end-user installation. This does not indicate whether it works with the current version of Blockland.

| Name | URL | Compatible version | Ready-to-install | Description |
| ------ | ------ | ------ | ------ | ------ |
| AuthUnlock | https://gitlab.com/Redo0/authunlock | ? | Yes ✓ | Lets you join internet games in offline mode. |
| BinaryFileIO | https://github.com/Pah1023/BinaryFileIO | **r2033** ✓ | Yes ✓ | Interact with files in binary mode using hexadecimal instead of strings |
| BLsql | https://gitlab.com/Eagle517/blsql | **r2033** ✓ | Yes ✓ | Interact with SQLite3 databses through TorqueScript |
| BlLua3 | https://notabug.org/redo/bllua3 | **r2033** ✓ | Yes ✓ | Simple Lua interface for TorqueScript (**experimental**) |
| BLpython3 | https://notabug.org/Queuenard/BLpython3 | **r2033** ✓ | Yes ✓ | Interface that can run Python scripts that interact with TorqueScript (**experimental**) |
| DataChunker Fix | https://gitlab.com/Eagle517/datachunker-fix | **r2033** ✓ | Yes ✓ | Fixes memory allocation bug. Particularly when many functions are defined. |
| Float Precision | https://gitlab.com/Eagle517/float-precision | **r2033** ✓ | Yes ✓ | Improves float precision in certain script scenarios |
| Ghost Buddy | https://notabug.org/Queuenard/GhostBuddy | **r2033** ✓ | Yes ✓ | Disables brick ghosting and ghosts bricks in an alternative way |
| Ghost Limit Bypass | https://gitlab.com/Eagle517/ghost-limit-bypass | **r2033** ✓ | Yes ✓ | Increases ghosting speed by maintaining "fast packet" mode permanently. |
| Immediate Refocus | https://gitlab.com/Eagle517/immediate-refocus | **r2033** ✓ | Yes ✓ | Improves speed of switching focus back to Blockland's window |
| Move Handler | https://gitlab.com/Eagle517/move-handler | **r2033** ✓ | Yes ✓ | A DLL that lets you see the moves of players and control the moves of AIPlayers. |
| Move Projectile | https://gitlab.com/Queuenard/move-projectile | **r2033** ✓ | Yes ✓ | A DLL that provides a TorqueScript callback for Projectile::processTick. |
| Network Priority | https://gitlab.com/Eagle517/network-priority | **r2033** ✓ | Yes ✓ | A DLL that allows for control of ghosting priorities for ghostable objects through TorqueScript. |
| NoBrickTransitions | https://github.com/Electrk/NoBrickTransitions | **r2033** ✓ | Yes ✓ | Removes the ugly white flash when planting bricks, the fade when changing a brick's color, and the fade when setting whether a brick is rendering. |
| NoFocus | https://gitlab.com/Redo0/nofocus | **r2033** ✓ | Yes ✓ | Stops Blockland from grabbing the focus when starting or leaving a server. **Older DLL, but confirmed working for r2033 as this DLL only interferes with external function calls.** |
| No Sleep | https://gitlab.com/Eagle517/no-sleep | **r2033** ✓ | Yes ✓ | Stops Blockland from running slower in the background |
| Object Collision | https://gitlab.com/Eagle517/object-collision | **r2033** ✓ | Yes ✓ | A DLL that provides a TorqueScript callback every time an object collides. |
| On-Game-Tick | https://gitlab.com/Eagle517/on-game-tick/ | **r2033** ✓ | Yes ✓ | Adds a TorqueScript callback that gets called every tick |
| PackageAnyFunction | https://gitlab.com/Queuenard/packageanyfunction | **r2033** ✓ | Yes ✓ | Lets you package or overwrite any function from TorqueScript. |
| Player Data Fields | https://gitlab.com/Eagle517/player-data-fields | **r2033** ✓ | Yes ✓ | Change datablock variables on individual Player objects |
| Projectile Curves | https://gitlab.com/Eagle517/projectile-curves | **r2033** ✓ | Yes ✓ | A server- and client-sided implementation of curving projectiles. Particularly used on BCS servers. When used with a client, this allows for accurate projectile movements. |
| Precise Timer | https://gitlab.com/Queuenard/PreciseTimer | **r2033** ✓ | Yes ✓ | Track the execution time of TorqueScript code. |
| Precipitation | https://gitlab.com/Eagle517/precipitation | **r2033** ✓  | Yes ✓ | A DLL that makes precipitation in Blockland collide with bricks that have raycasting on. This affects your client only. |
| Render Optimizations | https://gitlab.com/Eagle517/render-optimizations | **r2033** WIP | No | A work-in-progress DLL that increases overall 3D rendering performance |
| Selective Ghosting | https://gitlab.com/Eagle517/selective-ghosting | **r2033** ✓ | Yes ✓ | A DLL that allows you to ghost individual objects to clients in Blockland. |
| Shapename Data | https://gitlab.com/Queuenard/shapename-data | **r2033** ✓ | Yes ✓ | A DLL that records shapename data in Torque variables. This can be used to record screenshot metadata. |
| Texture Bleed | https://gitlab.com/Eagle517/texture-bleed | **r2033** ✓ | Yes ✓  | A DLL that modifies texture behavior with regards to edges |
| TraceBuddy | https://gitlab.com/Queuenard/TraceBuddy | **r2033** ✓ | Yes ✓  | A DLL that adds custom "trace2" function and dumps recent trace log to file, even if the server crashes. |
| Tracy | https://gitlab.com/Eagle517/bl-tracy | **r2033** ✓ | Yes ✓  | A DLL that integrates Tracy for profiling TorqueScript |
| Wine Fix | https://gitlab.com/Eagle517/wine-fix | **r2033** ✓ | Yes ✓  | A DLL that modifies the console so it can work without wineconsole which was removed in Wine 5.18 |

## **DLLs** (v20)

| Name | URL | Compatible version | Ready-to-install | Description |
| ------ | ------ | ------ | ------ | ------ |
| PlayerShadows | https://gitlab.com/Redo0/playershadows | B4v21 | Yes ✓ | Enables shadows for players all the time |
| RenderDistance | https://gitlab.com/Redo0/renderdistance | B4v21 | Yes ✓ | Huge render distances in Blockland v20; Stops the game from crashing when rendering tons of terrain |

## **DLLs** (abandoned)
DLLs that are abandoned and (likely or certainly) not working and no updated version exists.

| Name | URL | Compatible version | Ready-to-install | Description |
| ------ | ------ | ------ | ------ | ------ |
| BlocklandJS | https://github.com/hatf0/blocklandjs | ? | Yes ✓ | (2018) BlocklandJS - a implementation of V8 in Blockland. Bad code included. |
| NoFov | https://gitlab.com/Redo0/nofov | ? | Yes ✓ | Prevents the server from setting your FOV |

## **DLLs** (archived)
DLLs that no longer fit in one of the categories above, but used to belong there

| Name | URL | Compatible version | Ready-to-install | Description |
| ------ | ------ | ------ | ------ | ------ |
| BL-GhostLimitBypass | https://github.com/Pah1023/BL-GhostLimitBypass | ? | No | ? |
| BlocklandLua | https://github.com/qoh/BlocklandLua | ? | Yes ✓ | ? |
| GhostLimitBypass | https://github.com/Visolator/Blockland---FastPacket-DLL | ? | No | Sustains "fast packet" mode after clients have spawned. |
| PackageAnyFunction | https://gitlab.com/Redo0/packageanyfunction | r2005 | Yes ✓ | Lets you package or overwrite any function from TS. |
| PlayerCollisionDLL | https://github.com/Visolator/Blockland---PlayerCollisionDLL | ? | No | Exposes Torquescript functions to change the typemask of a SimObject and therefore allows selective collision. |
| SelectiveGhostingDLL | https://github.com/Visolator/Blockland---SelectiveGhostingDLL | ? | No | Exposes a function to Torquescript to change the net flags on a NetObject. |
| TraceEnabler | https://github.com/hatf0/TraceEnabler | **r2033** ✓ | No | A less laggy way to trace in TorqueScript. See "TraceBuddy" for something similar.  |


## **Libraries**
Non-runnable libraries for developers
| Name | URL | Description |
| ------ | ------ | ------ |
| libBLstructs | https://notabug.org/Queuenard/libBLstructs | Header-only library for Blockland DLL developers to handle known object layouts in memory |

## **Uncategorized**

https://github.com/bansheerubber/torquescript-interpreter

An in-progress TorqueScript interpreter. Not necessarily related to Blockland.

<br/><br/>

https://github.com/Pah1023/BL-Browser-CEF

BL-Browser, which allows displaying of web content on print bricks. I do not know whether this functions and there isn't much documentation.

<br/><br/>

https://gitlab.com/Eagle517/blockland-registry-tool

"A simple Windows program to configure Blockland's registry keys"

<br/><br/>

https://github.com/Electrk/dso.js

DSO decompiler for Blockland

<br/><br/>

https://github.com/Electrk/bl-decompiled

Blockland's default scripts decompiled

<br/><br/>

https://github.com/hatf0/PSGFY

Python DSO decompiler for Blockland (dead link)

<br/><br/>

https://notabug.org/Queuenard/ObjectTrace

Work in progress memory allocation/deallocation journal
